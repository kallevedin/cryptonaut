package main

import (
	".."
	"fmt"
	"net"
	"strconv"
	"time"
)

var (
	terminateProgram chan bool
)

func main() {
	addr := "127.0.0.1"
	port := 9999

	fmt.Println(addr + ":" + strconv.Itoa(port))
	cryptonaut.StartTCPProxy(addr, port)

	dnsServ, err := cryptonaut.StartDNSServer(nil, net.IPAddr{IP: net.ParseIP("127.0.0.1")}, 9192)
	if err != nil {
		fmt.Println(err)
		return
	}

	timeout := time.After(1000 * time.Second)
	for {
		select {
		case <-terminateProgram:
			return
		case <-timeout:
			fmt.Println("Terminating DNS server...")
			dnsServ.Terminate()
			break
		}
	}
}
