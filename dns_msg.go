package cryptonaut

import (
	"bytes"
	"encoding/binary"
	"errors"
	"net"
	"strings"
	"unicode"
)

const (
	MAX_DOMAIN_NAME_LEN = 255
	MAX_DNS_LABEL_LEN   = 63
	MAX_DNS_UDP_PKT_LEN = 512
)

type DNSMsg struct {
	Header      *DNSMsgHeader
	Questions   []*DNSMsgQuestion
	Answers     []*DNSMsgResourceRecord
	Authoritive []*DNSMsgResourceRecord
	Additionals []*DNSMsgResourceRecord
}

// Does the DNSMsg look kindof okay without looking too deep in the data structures.
func (this *DNSMsg) IsTopmostStructuresValid() bool {
	if this.Header == nil {
		return false
	}

	if this.Header.OPCODE > DNS_OPCODE_MAXVAL {
		return false
	}

	if this.Header.RCODE > DNS_RCODE_MAXVAL {
		return false
	}

	// also nice code duplication

	if this.Questions != nil {
		if this.Header.NumberOfQuestions() != len(this.Questions) {
			return false
		}
		for _, q := range this.Questions {
			if q == nil {
				return false
			}
		}
	} else if this.Header.NumberOfQuestions() != 0 {
		return false
	}

	if this.Answers != nil {
		if this.Header.NumberOfAnswers() != len(this.Answers) {
			return false
		}
		for _, q := range this.Questions {
			if q == nil {
				return false
			}
		}
	} else if this.Header.NumberOfAnswers() != 0 {
		return false
	}

	if this.Authoritive != nil {
		if this.Header.NumberOfAuthoritive() != len(this.Authoritive) {
			return false
		}
		for _, q := range this.Questions {
			if q == nil {
				return false
			}
		}
	} else if this.Header.NumberOfAuthoritive() != 0 {
		return false
	}

	if this.Additionals != nil {
		if this.Header.NumberOfAdditionals() != len(this.Additionals) {
			return false
		}
		for _, q := range this.Additionals {
			if q == nil {
				return false
			}
		}
	} else if this.Header.NumberOfAdditionals() != 0 {
		return false
	}

	return true
}

// Do not call this method unless you know that IsTopmostStructuresValid() returns true.
func (this *DNSMsg) ToBinary() ([]byte, error) {
	numSections := 1 + len(this.Questions) + len(this.Answers) + len(this.Authoritive) + len(this.Additionals)
	sections := make([][]byte, numSections)
	sectionIndex := 0

	bHeader, err := this.Header.ToBinary()
	if err != nil {
		return nil, err
	}
	sections[0] = bHeader
	sectionIndex++

	// hello code-duplication. is there no beautiful way to avoid this?

	for i := 0; i != len(this.Questions); i++ {
		rr, err := this.Questions[i].ToBinary()
		if err != nil {
			return nil, err
		}

		sections[sectionIndex] = rr
		sectionIndex++
	}

	for i := 0; i != len(this.Answers); i++ {
		rr, err := this.Answers[i].ToBinary()
		if err != nil {
			return nil, err
		}

		sections[sectionIndex] = rr
		sectionIndex++
	}

	for i := 0; i != len(this.Authoritive); i++ {
		rr, err := this.Authoritive[i].ToBinary()
		if err != nil {
			return nil, err
		}

		sections[sectionIndex] = rr
		sectionIndex++
	}

	for i := 0; i != len(this.Additionals); i++ {
		rr, err := this.Additionals[i].ToBinary()
		if err != nil {
			return nil, err
		}

		sections[sectionIndex] = rr
		sectionIndex++
	}

	return bytes.Join(sections, []byte{}), nil
}

func DNSMsgFromBinary(b []byte) (*DNSMsg, error) {
	return nil, nil // TODO
}

type DNSMsgHeader struct {
	ID      uint16    // id number assigned for this request-response (set by client)
	QR      bool      // false = query, true = response (set by both client and server)
	OPCODE  DNSOpcode // type of operation requested by client (set by client)
	AA      bool      // true if the responding dns server owns the (first?) name in the question section (set by server)
	TC      bool      // true = the message have been truncated - the client probably will switch to tcp if this happens (set by server)
	RD      bool      // recursion desired (set by client)
	RA      bool      // recursion available (set by server)
	RCODE   DNSRCode  // error codes (set by server)
	QDCOUNT uint16    // number of questions (len of DNSMsg.Questions)
	ANCOUNT uint16    // number of answers (len of DNSMsg.Answers)
	NSCOUNT uint16    // number of name server records (len of DNSMsg.Authority)
	ARCOUNT uint16    // number of additional records (len of DNSMsg.Additional)
}

// Decodes a 12-byte long byte array into a DNS message header.
// Performs no error checks except for length and nil.
func DNSMsgHeaderFromBinary(b []byte) (*DNSMsgHeader, error) {
	if b == nil || len(b) != 12 {
		return nil, errors.New("Invalid DNS header. It should be 12 bytes")
	}

	binOpt := binary.BigEndian.Uint16(b[2:4])

	header := &DNSMsgHeader{
		ID: binary.BigEndian.Uint16(b[0:2]),

		// binary option fields
		QR:     (binOpt & 0x8000) != 0,            // bit 0
		OPCODE: DNSOpcode((binOpt & 0x7800) >> 8), // bits 1-4
		AA:     (binOpt & 0x0400) != 0,            // bit 5
		TC:     (binOpt & 0x0200) != 0,            // bit 6
		RD:     (binOpt & 0x0100) != 0,            // bit 7
		RA:     (binOpt & 0x0080) != 0,            // bit 8
		// 3 zero-bits that we ignore: bit 9-11
		RCODE: DNSRCode(binOpt & 0x000F), // bits 12-15

		QDCOUNT: binary.BigEndian.Uint16(b[4:6]),
		ANCOUNT: binary.BigEndian.Uint16(b[6:8]),
		NSCOUNT: binary.BigEndian.Uint16(b[8:10]),
		ARCOUNT: binary.BigEndian.Uint16(b[10:12]),
	}

	return header, nil
}

// Encodes the DNS message header into binary form.
// Can return error if the Opcode or RCODE can not fit into their fields.
func (this *DNSMsgHeader) ToBinary() ([]byte, error) {
	if this.OPCODE > DNS_OPCODE_MAXVAL {
		return nil, errors.New("Opcode out of range - it is a " + string(int(DNS_OPCODE_BITS)) + "-bit wide field")
	}
	if this.RCODE > DNS_RCODE_MAXVAL {
		return nil, errors.New("RCODE out of range - it is a " + string(int(DNS_RCODE_BITS)) + "-bit wide field")
	}

	// create the binary options field
	var binOpt uint16 = 0
	if this.QR {
		binOpt |= 0x8000 // bit 0
	}
	binOpt |= uint16(this.OPCODE << 14) // bits 1-4
	if this.AA {
		binOpt |= 0x0400 // bit 5
	}
	if this.TC {
		binOpt |= 0x0200 // bit 6
	}
	if this.RD {
		binOpt |= 0x0100 // bit 7
	}
	if this.RA {
		binOpt |= 0x0080 // bit 8
	}
	// bits 9-11 will always be zero
	binOpt |= uint16(this.RCODE) // bits 12-15

	// put in place and done :)
	binHdr := make([]byte, 12)
	binary.BigEndian.PutUint16(binHdr[0:2], this.ID)
	binary.BigEndian.PutUint16(binHdr[2:4], binOpt)
	binary.BigEndian.PutUint16(binHdr[4:6], this.QDCOUNT)
	binary.BigEndian.PutUint16(binHdr[6:8], this.ANCOUNT)
	binary.BigEndian.PutUint16(binHdr[8:10], this.NSCOUNT)
	binary.BigEndian.PutUint16(binHdr[10:12], this.ARCOUNT)

	return binHdr, nil
}

// Typically only true for messages going from the client to the DNS server(s)
func (this *DNSMsgHeader) IsRequest() bool {
	return this.QR
}

// Typically only true for messages going back to the client
func (this *DNSMsgHeader) IsResponse() bool {
	return !this.QR
}

// What operation is requested? (standard or inverse query? or get server status?)
func (this *DNSMsgHeader) Opcode() DNSOpcode {
	return this.OPCODE
}

// True if the message could not fit into a UDP packet - the client should probably switch to TCP and ask again
func (this *DNSMsgHeader) IsTruncated() bool {
	return this.TC
}

// Client says it wants recursive DNS lookups
func (this *DNSMsgHeader) IsRecursionDesired() bool {
	return this.RD
}

// True if the server says that recursion is available
func (this *DNSMsgHeader) IsRecursionAvailable() bool {
	return this.RA
}

// The status code is typically set by the server to indicate an error or something like that
func (this *DNSMsgHeader) StatusCode() DNSRCode {
	return this.RCODE
}

// Typically always 1. The server keeps the clients question in the response.
func (this *DNSMsgHeader) NumberOfQuestions() int {
	return int(this.QDCOUNT)
}

// Is typically zero when the client sends a query. In most cases it becomes 1 when the server replies.
// (Unless it is a reverse query.)
func (this *DNSMsgHeader) NumberOfAnswers() int {
	return int(this.ANCOUNT)
}

// Could be non-zero but this implementation keeps it simple
func (this *DNSMsgHeader) NumberOfAuthoritive() int {
	return int(this.NSCOUNT)
}

// Could be non-zero but this implementation keeps it simple
func (this *DNSMsgHeader) NumberOfAdditionals() int {
	return int(this.ARCOUNT)
}

type DNSOpcode uint8

const (
	DNS_OPCODE_STANDARD_QUERY        DNSOpcode = 0
	DNS_OPCODE_INVERSE_QUERY         DNSOpcode = 1
	DNS_OPCODE_SERVER_STATUS_REQUEST DNSOpcode = 2

	DNS_OPCODE_BITS   = 4
	DNS_OPCODE_MAXVAL = 15
)

type DNSRCode uint8

const (
	DNS_RCODE_NO_ERROR        DNSRCode = 0
	DNS_RCODE_FORMAT_ERROR    DNSRCode = 1
	DNS_RCODE_SERVER_FAILURE  DNSRCode = 2
	DNS_RCODE_NAME_ERROR      DNSRCode = 3
	DNS_RCODE_NOT_IMPLEMENTED DNSRCode = 4
	DNS_RCODE_REFUSED         DNSRCode = 5

	DNS_RCODE_BITS   = 4
	DNS_RCODE_MAXVAL = 15
)

type DNSMsgQuestion struct {
	QNAME  string
	QTYPE  DNSQType
	QCLASS DNSQClass
}

type DNSQClass uint16

const (
	DNS_QCLASS_Internet DNSQClass = 1
	DNS_QCLASS_CSNET    DNSQClass = 2
	DNS_QCLASS_CHAOS    DNSQClass = 3
	DNS_QCLASS_Heisod   DNSQClass = 4
	DNS_QCLASS_ANY      DNSQClass = 255
)

// Given the offset where to start decoding, the number of questions in the question section (QDQOUNT) and a byte array,
// this function returns the length of the question section and the entries in the question section - or an error.
// (The length of the question section can only be determined by successfully decoding it. This gives the offset to the
// next section.)
func DNSMsgQuestionSectionDecode(offset, QDCOUNT int, b []byte) (length int, questions []*DNSMsgQuestion, err error) {
	if b == nil {
		return 0, nil, errors.New("Byte array is nil")
	}
	if offset > len(b) || offset < 0 {
		return 0, nil, errors.New("Offset out of bounds")
	}

	questions = make([]*DNSMsgQuestion, QDCOUNT)

	qOffset := offset
	for q := 0; q != QDCOUNT; q++ {
		domainName, domainNameLen, err := DNSDomainDecode(b[offset+qOffset:])
		if err != nil {
			return 0, nil, err
		}
		nameEnd := offset + qOffset + domainNameLen
		qEndOffset := nameEnd + 4

		if qEndOffset > len(b) {
			return 0, nil, errors.New("Invalid question section - maybe it is truncated")
		}

		qtype := DNSQType(binary.BigEndian.Uint16(b[nameEnd : nameEnd+2]))
		qclass := DNSQClass(binary.BigEndian.Uint16(b[nameEnd+2 : nameEnd+4]))

		questions[q] = &DNSMsgQuestion{
			QNAME:  domainName,
			QTYPE:  qtype,
			QCLASS: qclass,
		}

		qOffset += qEndOffset
	}
	return qOffset, questions, nil
}

func (this *DNSMsgQuestion) ToBinary() ([]byte, error) {
	return nil, nil
}

const DNSMsgResourceRecord_MIN_LEN = 11

type DNSMsgResourceRecord struct {
	NAME     string
	TYPE     DNSType
	CLASS    DNSClass // will always be 1 (1 = the internet)
	TTL      uint32   // how long to cache the resource record before it should be considered invalid, in seconds
	RDLENGTH uint16   // length of RDATA
	RDATA    []byte   // depends on TYPE and CLASS
}

type DNSClass uint16

const (
	DNS_CLASS_Internet DNSClass  = 1
	DNS_CLASS_CSNET    DNSQClass = 2
	DNS_CLASS_CHAOS    DNSQClass = 3
	DNS_CLASS_Heisod   DNSQClass = 4
)

func DNSMsgResourceRecordFromBinary(b []byte) (rr *DNSMsgResourceRecord, rrLen int, err error) {
	if b == nil || len(b) < DNSMsgResourceRecord_MIN_LEN {
		return nil, 0, errors.New("A DNS resource record must be at least " + string(int(DNSMsgResourceRecord_MIN_LEN)) + " bytes long")
	}

	name, nameEnd, err := DNSDomainDecode(b)
	if err != nil {
		return nil, 0, err
	}

	if len(b) > nameEnd+DNSMsgResourceRecord_MIN_LEN {
		return nil, 0, errors.New("Resource record is truncated or malformed")
	}

	rr = &DNSMsgResourceRecord{
		NAME:     name,
		TYPE:     DNSType(binary.BigEndian.Uint16(b[nameEnd : nameEnd+2])),
		CLASS:    DNSClass(binary.BigEndian.Uint16(b[nameEnd+2 : nameEnd+4])),
		TTL:      binary.BigEndian.Uint32(b[nameEnd+4 : nameEnd+8]),
		RDLENGTH: binary.BigEndian.Uint16(b[nameEnd+8 : nameEnd+10]),
	}

	rrLen = nameEnd + 10 + int(rr.RDLENGTH)
	if rrLen > len(b) {
		return nil, 0, errors.New("Resource records RDLENGTH is out of bounds")
	}
	rr.RDATA = b[nameEnd+10 : rrLen]

	return rr, rrLen, nil
}

func (this *DNSMsgResourceRecord) ToBinary() ([]byte, error) {
	name, err := DNSDomainEncode(this.NAME)
	if err != nil {
		return nil, err
	}

	nameLen := len(name)
	rrLen := len(name) + 2 + 2 + 4 + 2 + len(this.RDATA)
	rrBytes := make([]byte, rrLen)

	copy(rrBytes[:nameLen], name)
	binary.BigEndian.PutUint16(rrBytes[nameLen:nameLen+2], uint16(this.TYPE))
	binary.BigEndian.PutUint16(rrBytes[nameLen+2:nameLen+4], uint16(this.CLASS))
	binary.BigEndian.PutUint32(rrBytes[nameLen+4:nameLen+8], this.TTL)
	binary.BigEndian.PutUint16(rrBytes[nameLen+8:nameLen+10], this.RDLENGTH)
	copy(rrBytes[nameLen+10:], this.RDATA)

	return rrBytes, nil
}

// identifies the type of a resource record - typically only in replies from DNS server
type DNSType uint16

const (
	DNS_Type_A     DNSType = 1  // get ipv4 address of domain name
	DNS_Type_NS    DNSType = 2  // get name server for domain
	DNS_Type_CNAME DNSType = 5  // get canonical name
	DNS_Type_WKS   DNSType = 11 // what ports are open (=what services are provided)
	DNS_Type_PTR   DNSType = 12 // get pointer
	DNS_Type_HINFO DNSType = 14 // get host info
	DNS_Type_MX    DNSType = 15 // get mail server
	DNS_Type_TXT   DNSType = 16 // get generic text associated with domain
)

// identifies what kind of query a dns request is
type DNSQType uint16

const (
	DNS_QType_A     DNSType  = 1   // get ipv4 address of domain name
	DNS_QType_NS    DNSType  = 2   // get name server for domain
	DNS_QType_CNAME DNSType  = 5   // get canonical name
	DNS_QType_WKS   DNSType  = 11  // what ports are open (=what services are provided)
	DNS_QType_PTR   DNSType  = 12  // get pointer
	DNS_QType_HINFO DNSType  = 14  // get host info
	DNS_QType_MX    DNSType  = 15  // get mail server
	DNS_QType_TXT   DNSType  = 16  // get generic text associated with domain
	DNS_QType_AXFR  DNSQType = 252 // request to transfer entire zone
	DNS_QType_ALL   DNSQType = 255 // request of all record types available
)

// The type A resource record
type DNSRDataA struct {
	IPv4 net.IP
}

func (this *DNSRDataA) ToBinary() ([]byte, error) {
	if len(this.IPv4) != 4 {
		return nil, errors.New("Not an IPv4 address")
	}
	return []byte(this.IPv4), nil
}

func DNSRDataAFromBinary(b []byte) (*DNSRDataA, error) {
	if b == nil || len(b) != 4 {
		return nil, errors.New("Invalid RDATA type A record")
	}
	return &DNSRDataA{IPv4: net.IP(b)}, nil
}

// The name server (DNS server) that is responsible for the domain.
// The record consists only of a DNS Domain Name (use DNSDomainEncode() and DNSDomainDecode())
type DNSRDataNS string

// Canonical name resource record. Kindof like an alias or a link to another domain, often meaning that one should go to the domain referenced by CNAME instead of using this domain.
// The record consists only of a DNS Domain Name (use DNSDomainEncode() and DNSDomainDecode())
type DNSRDataCNAME string

// A pointer to another domain.
// The record consists only of a DNS Domain Name (use DNSDomainEncode() and DNSDomainDecode())
type DNSRDataPTR string

const DNSRDataMX_MIN_LEN = 3

// Describes a mail server
type DNSRDataMX struct {
	Preference uint16 // Used to hint about which mail server to prefer. 0 is best. 2^16-1 is worst.
	DomainName string // The name of the mail server
}

func (this *DNSRDataMX) ToBinary() ([]byte, error) {
	name, err := DNSDomainEncode(this.DomainName)
	if err != nil {
		return nil, err
	}
	buf := make([]byte, 2+len(name))
	binary.BigEndian.PutUint16(buf[0:2], this.Preference)
	copy(buf[2:], name)

	return buf, nil
}

func DNSRDataMXFromBinary(b []byte) (*DNSRDataMX, error) {
	if b == nil || len(b) < DNSRDataMX_MIN_LEN {
		return nil, errors.New("Invalid RDATA type MX record")
	}

	pref := binary.BigEndian.Uint16(b[0:2])

	name, nameLen, err := DNSDomainDecode(b[2:])
	if err != nil {
		return nil, err
	}
	if nameLen+2 != len(b) {
		return nil, errors.New("Invalid RDATA type MX record: There is extra unused space after the mx domain name")
	}

	rr := &DNSRDataMX{
		Preference: pref,
		DomainName: name,
	}

	return rr, nil
}

type DNSRDataTXT struct{}

// Converts a string to what is called <character-string> in RFC1035: The first byte is the length of the string, which
// is followed by the string. The string must be in ASCII and at most 255 chars long.
func MakeDNSCharacterString(s string) ([]byte, error) {
	len := 0
	for _, r := range s {
		if r > unicode.MaxASCII {
			return nil, errors.New("Invalid character string - must be ASCII")
		}
		len += 1
	}
	if len > 255 {
		return nil, errors.New("Invalid character string - must be max 255 chars long")
	}
	dnsCharStr := make([]byte, len+1)
	dnsCharStr[0] = byte(len)
	copy(dnsCharStr[1:], []byte(s))

	return dnsCharStr, nil
}

func FromDNSCharacterString(b []byte) (str string, length int, err error) {
	length = int(b[0])
	if length > len(b)-1 {
		return "", 0, errors.New("Malformed character-string")
	}
	for i := 0; i != length; i++ {
		if b[i] > unicode.MaxASCII {
			return "", 0, errors.New("Malformed character-string contains illegal characters")
		}
	}
	return string(b[:length]), length, nil
}

// Converts a domain name (fqdn) to the binary representation used in DNS messages.
// This will not punycode your domain name for you - it only handles oldskool domain names
func DNSDomainEncode(name string) ([]byte, error) {
	// see DNSDomainDecode() for how this format works

	for _, r := range name {
		if !IsValidRuneInDNSLabel(r) {
			return nil, errors.New("Invalid domain name \"" + name + "\". Invalid character: " + string(r))
		}
	}

	if len(name) > MAX_DOMAIN_NAME_LEN {
		return nil, errors.New("Invalid domain name \"" + name + "\". Too long. Max length is " + string(int(MAX_DOMAIN_NAME_LEN)) + " chars")
	}

	substrings := strings.Split(name, ".")
	numberOfEntries := (len(substrings) * 2) + 1
	entries := make([][]byte, numberOfEntries)

	for i, substr := range substrings {
		if len(substr) > MAX_DNS_LABEL_LEN {
			return nil, errors.New("Invalid domain name \"" + name + "\". Label \"" + substr + "\" is too long. Max label-length is " + string(int(MAX_DNS_LABEL_LEN)) + " chars")
		}
		entries[i*2] = make([]byte, 1)
		entries[i*2][0] = byte(len(substr))

		entries[(i*2)+1] = []byte(substr)
	}

	entries[numberOfEntries] = make([]byte, 1)
	entries[numberOfEntries][0] = 0

	return bytes.Join(entries, []byte{}), nil
}

// Decodes the binary format of domain names used in DNS messages from a byte slice.
// Returns the domain name, the length of the domain name in bytes - or an error
func DNSDomainDecode(b []byte) (domainName string, byteLength int, err error) {
	/*
		domain names are encoded like this:

		repeat for each label
			one byte telling how many bytes the label is
			a label that is that many bytes long (ascii-encoded)
		then at the end a single byte with value 0

		so "abc.de.fg" would be encoded as: [3, "abc", 2, "de", 2, "fg", 0]
	*/

	var buffer bytes.Buffer
	labelOffset := 0
	for {
		labelLen := int(b[labelOffset])
		if labelLen == 0 {
			break
		}
		if labelOffset+labelLen > len(b) {
			return "", 0, errors.New("Invalid DNS encoding: Index out of bounds")
		}
		for _, r := range b[labelOffset : labelOffset+labelLen] {
			if !IsValidRuneInDNSLabel(rune(r)) {
				return "", 0, errors.New("Invalid DNS name encoding: Invalid character")
			}
		}
		buffer.WriteString(string(b[labelOffset : labelOffset+labelLen]))
		buffer.WriteRune('.')
		labelOffset += (1 + labelLen)
	}
	return buffer.String(), labelOffset + 1, nil
}

// Is true if the rune is okay to send over the network.
func IsValidRuneInDNSLabel(r rune) bool {
	return r > unicode.MaxASCII || !unicode.IsLetter(r) && !unicode.IsNumber(r) && r != '-'
}
