package cryptonaut

import (
	"fmt"
	"net"
	"sync"
)

const (
	CONCURRENT_UDP_DNS_LOOKUPS = 32
	//CONCURRENT_TCP_DNS_LOOKUPS = 8
)

// Defines an instance of a DNS server that can be either running or dead.
type DNSServer struct {
	running        bool
	lAddr          net.IPAddr
	lPort          int
	udpConn        *net.UDPConn
	tcpListener    *net.TCPListener
	errorChan      chan error
	udpRequestChan chan *udpDnsRequest
	tcpRequestChan chan *net.TCPConn
	killChan       chan bool
	resolver       DNSResolver
	mutex          sync.RWMutex
}

// Mechanism provided by the caller to resolve domain names to IP addresses
type DNSResolver interface {
	Resolve(ipVersion int, recordType, name string) net.IP
}

type udpDnsRequest struct {
	messageLen int
	buffer     []byte
	rAddr      *net.UDPAddr
	socket     *net.UDPConn
}

// Creates and starts a new DNS server. It just handles the low-level DNS protocol, actual domain name resolution have
// to be provided by the caller via the DNSResolver interface.
func StartDNSServer(resolver DNSResolver, listenAddress net.IPAddr, listenPort int) (*DNSServer, error) {

	ip := net.ParseIP(listenAddress.String())

	udpAddr := net.UDPAddr{
		IP:   ip,
		Port: listenPort,
		Zone: listenAddress.Zone,
	}

	tcpAddr := net.TCPAddr{
		IP:   ip,
		Port: listenPort,
		Zone: listenAddress.Zone,
	}

	udpConn, err := net.ListenUDP("udp", &udpAddr)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	tcpListener, err := net.ListenTCP("tcp", &tcpAddr)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	dnsServer := &DNSServer{
		running:        true,
		lAddr:          listenAddress,
		lPort:          listenPort,
		udpConn:        udpConn,
		tcpListener:    tcpListener,
		errorChan:      make(chan error),
		udpRequestChan: make(chan *udpDnsRequest, CONCURRENT_UDP_DNS_LOOKUPS),
		tcpRequestChan: make(chan *net.TCPConn),
		killChan:       make(chan bool),
		resolver:       resolver,
		mutex:          sync.RWMutex{},
	}

	udpTerminatedChan := make(chan bool)
	tcpTerminatedChan := make(chan bool)
	go dnsServer.udpDnsListener(udpTerminatedChan)
	go dnsServer.tcpDnsListener(tcpTerminatedChan)
	go dnsServer.dnsServer(udpTerminatedChan, tcpTerminatedChan)

	return dnsServer, nil
}

// Is true if the DNS server is running
func (this *DNSServer) IsRunning() bool {
	this.mutex.RLock()
	defer this.mutex.RUnlock()

	return this.running
}

// Terminates a DNS running server. If it already is terminated, nothing happens.
// It is not possible to resume or start a DNS server after it is stopped - You will need to StartDNSServer() again.
func (this *DNSServer) Terminate() {
	this.mutex.Lock()
	defer this.mutex.Unlock()

	if !this.running {
		return
	}

	this.running = false
	if this.udpConn != nil {
		this.udpConn.Close()
	}
	if this.tcpListener != nil {
		this.tcpListener.Close()
	}

	this.killChan <- true
}

func (this *DNSServer) dnsServer(udpTerminatedChan, tcpTerminatedChan chan bool) {
	for {
		select {
		case err := <-this.errorChan:
			fmt.Println("Server got error: ", err)
			return
		case udpRequest := <-this.udpRequestChan:
			fmt.Println("Server got UDP request from: ", udpRequest.rAddr.String())
			break
		case tcpClient := <-this.tcpRequestChan:
			fmt.Println("Server got TCP request from: ", tcpClient.RemoteAddr().String())
			break
		case <-this.killChan:
			// wait for the listeners to die
			fmt.Println("Waiting for listeners to die")
			<-udpTerminatedChan
			<-tcpTerminatedChan
			fmt.Println("Listeners has died, terminating")
			return
		}
	}
}

// receives incomming tcp connections and forwards the connections to the dnsServer
func (this *DNSServer) tcpDnsListener(tcpTerminatedChan chan bool) {
	defer this.tcpListener.Close()
	defer close(this.tcpRequestChan)
	defer func() { tcpTerminatedChan <- true }()
	defer fmt.Println("TCP DNS listener terminating.")

	fmt.Println("TCP DNS listner up")

	for {
		conn, err := this.tcpListener.AcceptTCP()
		if err != nil {
			this.mutex.RLock()
			if !this.running {
				this.mutex.RUnlock()
				return
			}
			this.mutex.RUnlock()
			this.errorChan <- err
			return
		}

		this.tcpRequestChan <- conn
	}
}

// receives the incoming udp packets that the clients send us and forwards them to the dnsServer
func (this *DNSServer) udpDnsListener(udpTerminatedChan chan bool) {
	defer this.udpConn.Close()
	defer close(this.udpRequestChan)
	defer func() { udpTerminatedChan <- true }()
	defer fmt.Println("UDP DNS listener terminating.")
	buf := make([]byte, 4096)

	fmt.Println("UDP DNS listner up")

	for {
		bytesRead, remoteAddr, err := this.udpConn.ReadFromUDP(buf)

		if err != nil {
			this.mutex.RLock()
			if !this.running {
				this.mutex.RUnlock()
				return
			}
			this.mutex.RUnlock()
			this.errorChan <- err
			return
		}

		this.udpRequestChan <- &udpDnsRequest{
			messageLen: bytesRead,
			buffer:     buf,
			rAddr:      remoteAddr,
			socket:     this.udpConn,
		}
	}
}
